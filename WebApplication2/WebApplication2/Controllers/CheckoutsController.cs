﻿using Braintree;
using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MySql.Data.MySqlClient;
using WebApplication2.Models;
using System.Net;
using System.Net.Mail;

namespace WebApplication2.Controllers
{
    public class CheckoutsController : Controller
    {
        string strcon = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        public IBraintreeConfiguration config = new BraintreeConfiguration();
        
        public static readonly TransactionStatus[] transactionSuccessStatuses = {
                                                                                    TransactionStatus.AUTHORIZED,
                                                                                    TransactionStatus.AUTHORIZING,
                                                                                    TransactionStatus.SETTLED,
                                                                                    TransactionStatus.SETTLING,
                                                                                    TransactionStatus.SETTLEMENT_CONFIRMED,
                                                                                    TransactionStatus.SETTLEMENT_PENDING,
                                                                                    TransactionStatus.SUBMITTED_FOR_SETTLEMENT
                                                                                };
        public ActionResult New()
        {
            var gateway = config.GetGateway();
            var clientToken = gateway.ClientToken.generate();
            string price = Session["amount"] as string;
            ViewBag.ClientToken = clientToken;
            ViewBag.price = price.Substring(1); 
            return View();
        }

        public ActionResult Create()
        {
            var gateway = config.GetGateway();
            Decimal amount;

            try
            {
                amount = Convert.ToDecimal(Request["amount"]);
            }
            catch (FormatException e)
            {
                TempData["Flash"] = "Error: 81503: Amount is an invalid format.";
                return RedirectToAction("New");
            }
            
            var nonce = Request["payment_method_nonce"];
            var request = new TransactionRequest
            {
                Amount = amount,
                PaymentMethodNonce = nonce,
                Options = new TransactionOptionsRequest
                {
                    SubmitForSettlement = true
                }
            };

            Result<Transaction> result = gateway.Transaction.Sale(request);
            if (result.IsSuccess())
            {
                Transaction transaction = result.Target;
                return RedirectToAction("Show", new { id = transaction.Id });
            }
            else if (result.Transaction != null)
            {
                return RedirectToAction("Show", new { id = result.Transaction.Id });
            }
            else
            {
                string errorMessages = "";
                foreach (ValidationError error in result.Errors.DeepAll())
                {
                    errorMessages += "Error: " + (int)error.Code + " - " + error.Message + "\n";
                }
                TempData["Flash"] = errorMessages;
                return RedirectToAction("New");
            }
        }

        public ActionResult Show(String id)
        {
            string price = Session["amount"] as string;
            price = price.Substring(1);
            
            var gateway = config.GetGateway();
            Transaction transaction = gateway.Transaction.Find(id);
            if (transactionSuccessStatuses.Contains(transaction.Status))
            {
                TempData["header"] = "Sweet Success!";
                TempData["icon"] = "success";
                TempData["message"] = "Your test transaction has been successfully processed. See the Braintree API response and try again.";

                string bustype = Session["entityType"] as string;
                switch (bustype)
                {
                    case "company":
                        Insert2DB("company");
                        break;
                    case "partnership":
                        Insert2DB("partnership");
                        break;
                    case "na":
                        Insert2DB("na");
                        break;
                    case "soleproprietor":
                        Insert2DB("soleproprietor");
                        break;
                    case "pam":
                        Insert2DB("pam");
                        break;
                    case "cpa":
                        Insert2DB("cpa");
                        break;
                    case "barcouncil":
                        Insert2DB("barcouncil");
                        break;
                }

                MailMessage msg = new MailMessage();

                msg.To.Add(new MailAddress("chinwk_95@hotmail.com"));
                msg.From = new MailAddress("mindserver15@gmail.com");
                msg.Subject = "Purchase Summary";
                msg.Body = "Your Payment is Successful. Please visit the following link: <a href=\"http://localhost:53429/Home/Contact \">Here</a>";
                msg.IsBodyHtml = true;

                using (SmtpClient smtp = new SmtpClient())
                {
                    NetworkCredential credential = new NetworkCredential("mindserver15@gmail.com", "proxyguy123");
                    smtp.Host = "smtp.gmail.com";
                    smtp.Port = 587;
                    smtp.EnableSsl = true;
                    smtp.Credentials = credential;
                    smtp.Send(msg);
                }
                
                using (MySqlConnection sqlcon = new MySqlConnection(strcon))
                {
                    string query = "INSERT INTO payment_transactions (payment_method, transaction_date, amount, bttransaction_id) VALUES ('Credit Card','" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "','" + price + "','" + id + "')";
                    using (MySqlCommand cmd = new MySqlCommand(query))
                    {
                        cmd.Connection = sqlcon;
                        sqlcon.Open();
                        cmd.ExecuteScalar();
                    }
                }
            }
            else
            {
                TempData["header"] = "Transaction Failed";
                TempData["icon"] = "fail";
                TempData["message"] = "Your test transaction has a status of " + transaction.Status + ". See the Braintree API response and try again.";
                
                MailMessage msg = new MailMessage();

                msg.To.Add(new MailAddress("chinwk_95@hotmail.com"));
                msg.From = new MailAddress("mindserver15@gmail.com");
                msg.Subject = "Purchase Summary";
                msg.Body = "Your Payment is Not Successful";
                msg.IsBodyHtml = true;

                using (SmtpClient smtp = new SmtpClient())
                {
                    NetworkCredential credential = new NetworkCredential("mindserver15@gmail.com", "proxyguy123");
                    smtp.Host = "smtp.gmail.com";
                    smtp.Port = 587;
                    smtp.EnableSsl = true;
                    smtp.Credentials = credential;
                    smtp.Send(msg);
                }
            };

            ViewBag.Transaction = transaction;
            return View();
        }

        public void Insert2DB(string dbTable) 
        {
            ItemDetails itemInfo = Session["itemInfo"] as ItemDetails;
            foreach (ItemDetails temp in itemInfo.detailList)
            {
                using (MySqlConnection sqlcon = new MySqlConnection(strcon))
                {
                    string query = "INSERT INTO dns_module (domain_name, country_code, purchase_date, expiration_date, customer_id, state_id, business_type) VALUES ('" + temp.Domain + "','" + temp.Domain.Substring(temp.Domain.Length-2, 2) + "','" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "','" + DateTime.Now.AddYears(1).ToString("yyyy-MM-dd HH:mm:ss") + "','CT0001A','Validation','" + dbTable + "')";
                    using (MySqlCommand cmd = new MySqlCommand(query))
                    {
                        cmd.Connection = sqlcon;
                        sqlcon.Open();
                        cmd.ExecuteScalar();
                    }
                    if (dbTable.Equals("na"))
                    {
                        query = "INSERT INTO " + dbTable + " (" + dbTable + "_owner_name, " + dbTable + "_owner_phone_number, " + dbTable + "_owner_email, " + dbTable + "_owner_ic_number, dns_id, business_type) VALUES ('" + temp.OwnerName + "','" + temp.OwnerPhoneNum + "','" + temp.OwnerEmail + "','" + temp.ICNum + "',(SELECT x.dns_id FROM dns_module x WHERE x.domain_name = '" + temp.Domain + "' AND x.country_code = '" + temp.Domain.Substring(temp.Domain.Length - 2, 2) + "'),'" + dbTable + "')";
                        using (MySqlCommand cmd = new MySqlCommand(query))
                        {
                            cmd.Connection = sqlcon;
                            cmd.ExecuteReader();
                        }
                    }
                    else
                    {
                        query = "INSERT INTO " + dbTable + " (" + dbTable + "_registration_number, " + dbTable + "_name, " + dbTable + "_address, " + dbTable + "_phone_number, " + dbTable + "_email, " + dbTable + "_owner_name, " + dbTable + "_owner_phone_number, " + dbTable + "_owner_email, " + dbTable + "_owner_ic_number, dns_id, business_type) VALUES ('" + temp.RegNum + "','" + temp.Name + "','" + temp.Address + "','" + temp.PhoneNum + "','" + temp.Email + "','" + temp.OwnerName + "','" + temp.OwnerPhoneNum + "','" + temp.OwnerEmail + "','" + temp.ICNum + "',(SELECT x.dns_id FROM dns_module x WHERE x.domain_name = '" + temp.Domain + "' AND x.country_code = '" + temp.Domain.Substring(temp.Domain.Length - 2, 2) + "'),'" + dbTable + "')";
                        using (MySqlCommand cmd = new MySqlCommand(query))
                        {
                            cmd.Connection = sqlcon;
                            cmd.ExecuteReader();
                        }
                    }
                }
            }        
        }
    }
}
