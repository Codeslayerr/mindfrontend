﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using MySql.Data.MySqlClient;
using PhoneNumbers;
using WebApplication2.Models;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        string strcon = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        private static List<string> cart = new List<string>();
        private static string dnsearch_param;

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult NewFillDetails()
        {
            ItemDetails itemInfo = new ItemDetails();
            itemInfo.detailList = new List<ItemDetails>();
            foreach (var item in Session["cart"] as List<string>)
            {
                itemInfo.detailList.Add(new ItemDetails(item));
            }
            Session["itemInfo"] = itemInfo;
            return RedirectToAction("FillDetails");
        }

        public ActionResult FillDetails()
        {
            ItemDetails itemInfo = Session["itemInfo"] as ItemDetails;
            return View(itemInfo);
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [HttpPost]
        public ActionResult SelectEntity(string entityType)
        {
            Session["entityType"] = entityType;
            return RedirectToAction("Search");
        }

        public ActionResult Search(string CountryCode, string search)
        {
            string search_dn, search_gtld, search_full;
            List<string> gtldDB = new List<string>();
            List<string> dn_suggest = new List<string>();
            List<string> supportedregions2 = GetCountryCodeList();

            ViewBag.country = supportedregions2;
            ViewBag.Message = "Search Your Domain Name";
            ViewBag.DisplayGTLD = GetBusinessGTLDFromDB(Session["entityType"] as string);

            if (!String.IsNullOrEmpty(search) && (!String.IsNullOrEmpty(CountryCode)))
            {
                dnsearch_param = search + CountryCode;
                search = search.ToLower();

                if (search.Contains(".") && search.Contains("google"))
                {
                    string[] search_arr = search.Split('.');
                    search_dn = search_arr[0];
                    search_gtld = "." + search_arr[1];
                    using (MySqlConnection sqlcon = new MySqlConnection(strcon))
                    {
                        string query = " SELECT tld_name FROM tld WHERE tld_name = '" + search_gtld + "' AND business_type='" + Session["entityType"] as string + "'";
                        using (MySqlCommand cmd = new MySqlCommand(query))
                        {
                            cmd.Connection = sqlcon;
                            sqlcon.Open();
                            using (MySqlDataReader reader = cmd.ExecuteReader())
                            {
                                if (!reader.Read())
                                {
                                    ViewBag.SearchStatus = "\"" + search_gtld + "\" is not catered for the selected Entity Type.";
                                    ViewBag.cart = cart;
                                    return View();
                                }
                            }
                        }
                    }

                    search_full = search + CountryCode;

                    using (MySqlConnection sqlcon = new MySqlConnection(strcon))
                    {
                        string query = " SELECT x.zone FROM domain_names x JOIN dns_module y ON x.nt_zone_id = y.nt_zone_id WHERE zone = '" + search_full + "' AND domain_name = '" + search_full + "'";
                        using (MySqlCommand cmd = new MySqlCommand(query))
                        {
                            cmd.Connection = sqlcon;
                            sqlcon.Open();
                            using (MySqlDataReader reader = cmd.ExecuteReader())
                            {
                                if (!reader.Read())
                                {
                                    ViewBag.SearchStatus = "Good news! The domain name \"" + search_full + "\"" + " is still available.";
                                    ViewBag.Available_dn = search_full;
                                    ViewBag.cart = cart;
                                    return View();
                                }
                                else
                                {
                                    gtldDB = GetBusinessGTLDFromDB(Session["entityType"] as string);
                                    dn_suggest = SearchSuggestion(search_dn, gtldDB, CountryCode);
                                    ViewBag.SearchStatus = "Sorry! The domain name you've searched \"" + search_full + "\"" + " is not available.";
                                    ViewBag.Suggestion = dn_suggest;
                                    ViewBag.cart = cart;
                                    return View();
                                }
                            }
                        }
                    }
                }
                else if (!search.Contains(".") && search.Contains("google"))
                {
                    gtldDB = GetBusinessGTLDFromDB(Session["entityType"] as string);
                    dn_suggest = SearchSuggestion(search, gtldDB, CountryCode);
                    ViewBag.SearchStatus = "Here are some suggestions for you";
                    ViewBag.Suggestion = dn_suggest;
                    ViewBag.cart = cart;
                    return View();
                }
                else
                {
                    ViewBag.SearchStatus = "Invalid domain name. The domain name you searched for must contain your business/company name in it.";
                    ViewBag.cart = cart;
                    return View();
                }
            }
            else
            {
                ViewBag.cart = cart;
                return View();
            }
        } 

        public List<string> GetBusinessGTLDFromDB (string entityType)
        {
            List<string> gtldDB = new List<string>();
            using (MySqlConnection sqlcon = new MySqlConnection(strcon))
            {
                string query = " SELECT tld_name FROM tld WHERE business_type='" + entityType + "'";
                using (MySqlCommand cmd = new MySqlCommand(query))
                {
                    cmd.Connection = sqlcon;
                    sqlcon.Open();
                    using (MySqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            gtldDB.Add(reader[0].ToString());
                        }
                    }
                }
            }

            return gtldDB;
        }

        public List<string> GetCountryCodeList ()
        {
            PhoneNumberUtil phoneutil = PhoneNumberUtil.GetInstance();
            List<String> supportedregions1 = new List<String>(phoneutil.GetSupportedRegions());
            List<String> supportedregions2 = new List<String>();

            supportedregions2.Add(".wo");

            foreach (String temp in supportedregions1.ToList())
            {
                supportedregions2.Add("." + temp.ToLower());
            }

            return supportedregions2;
        }

        public List<string> SearchSuggestion (string dName, List<string> gtldDB, string countryCode)
        {
            List<string> dName_suggest = RandomDN(dName);
            List<string> dn_suggest = new List<string>();

            string query, temp;
            foreach (string gtld in gtldDB)
            {
                foreach (string dn in dName_suggest)
                {
                    using (MySqlConnection sqlcon = new MySqlConnection(strcon))
                    {
                        temp = dn + gtld + countryCode;
                        query = " SELECT x.zone FROM domain_names x JOIN dns_module y ON x.nt_zone_id = y.nt_zone_id WHERE zone = '" + temp + "' AND domain_name = '" + temp + "'";
                        using (MySqlCommand cmd = new MySqlCommand(query))
                        {
                            cmd.Connection = sqlcon;
                            sqlcon.Open();
                            using (MySqlDataReader reader = cmd.ExecuteReader())
                            {
                                if (!reader.Read())
                                {
                                    dn_suggest.Add(temp);
                                }
                            }
                        }
                    }
                }
            }        
            return dn_suggest;
        }

        public List<string> RandomDN (string dName)
        {
            string[] random_word = new string[] { "123", "abc", "store", "web", "sale" };
            List<string> dName_suggest = new List<string>();

            dName_suggest.Add(dName);
            for (int i=0; i< random_word.Length; i++)
            {
                    dName_suggest.Add(dName + random_word[i]);
                    dName_suggest.Add(random_word[i] + dName);
            } 
            return dName_suggest;
        } 

        public ActionResult Cart()
        {
            string[] sum_arr = new string[3];
            double sumbook = 0, sumsub = 0;
            foreach (string item in cart)
            {
                if (item.Substring(item.Length-2, 2).Equals("wo"))
                {
                    sumbook += 99.99;
                    sumsub += 888.88;
                }
                else
                {
                    sumbook += 9.99;
                    sumsub += 88.88;
                }
            }
            sum_arr[0] = "$" + sumbook.ToString();
            ViewBag.cart = cart;
            ViewBag.sum = sum_arr;
            Session["amount"] = sum_arr[0];
            Session["cart"] = cart;
            return View();
        }

        [HttpPost]
        public ActionResult RmCart(string domain)
        {
            cart.Remove(domain);
            return RedirectToAction("Cart");
        }

        [HttpPost]
        public ActionResult AddCart(string domain)
        {
            cart.Add(domain);
            return RedirectToAction("Search", new { CountryCode = dnsearch_param.Substring(dnsearch_param.Length-3, 3), search = dnsearch_param.Substring(0, dnsearch_param.Length-3) });
        }

        [HttpPost]
        public ActionResult RemoveCart(string domain)
        {
            cart.Remove(domain);
            return RedirectToAction("Search", new { CountryCode = dnsearch_param.Substring(dnsearch_param.Length-3, 3), search = dnsearch_param.Substring(0, dnsearch_param.Length-3) });
        }

        public ActionResult Admin()
        {
            return RedirectToAction("Validate");
        }

        public ActionResult SaveDetails(ItemDetails submodel)
        {
            ItemDetails itemInfo = Session["itemInfo"] as ItemDetails;
            ItemDetails instance = itemInfo.detailList.FirstOrDefault(i => i.Domain == submodel.Domain);
            int index = itemInfo.detailList.IndexOf(instance);
            submodel.Edit = false;
            itemInfo.detailList[index] = submodel;
            Session["itemInfo"] = itemInfo;
            return RedirectToAction("FillDetails");
        }

        public ActionResult EditDetails(ItemDetails submodel)
        {
            ItemDetails itemInfo = Session["itemInfo"] as ItemDetails;
            ItemDetails instance = itemInfo.detailList.FirstOrDefault(i => i.Domain == submodel.Domain);
            int index = itemInfo.detailList.IndexOf(instance);
            submodel.Edit = true;
            itemInfo.detailList[index] = submodel;
            Session["itemInfo"] = itemInfo;
            return RedirectToAction("FillDetails");
        }

        public ActionResult About()
        {
            return View();
        }
    }
}
