﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication2.Models;
using System.Net.Mail;
using System.Net;
using System.Configuration;
using MySql.Data.MySqlClient;
using Braintree;

namespace WebApplication2.Controllers
{
    public class AdminController : Controller
    {
        string strcon = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

        public ActionResult ViewAcc()
        {
            return View();
        }

        public ActionResult DeleteAcc()
        {
            return View();
        }

        public ActionResult EditAcc()
        {
            return View();
        }
        [Authorize]
        public ActionResult Validate()
        {
            List<string[]> temp_list = new List<string[]>();
            using (MySqlConnection sqlcon = new MySqlConnection(strcon))
            {
                string query = "SELECT dns_id, domain_name, country_code, customer_id, business_type, state_id AS status from dns_module where state_id = 'Validation'";
                using (MySqlCommand cmd = new MySqlCommand(query))
                {
                    cmd.Connection = sqlcon;
                    sqlcon.Open();
                    using (MySqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            string[] temp_arr = new string[] { reader[0].ToString(), reader[1].ToString(), reader[2].ToString(), reader[3].ToString(), reader[4].ToString(), reader[5].ToString() };
                            temp_list.Add(temp_arr);
                        }
                        ViewBag.validate = temp_list;
                    }
                }
            }
            return View();
        }
        [Authorize]
        public ActionResult Approve(string dname, string countryCode)
        {
            using (MySqlConnection sqlcon = new MySqlConnection(strcon))
            {
                string query = "INSERT INTO domain_names (nt_group_id, zone, deleted) VALUES (1, '" + dname + "', 0)";
                using (MySqlCommand cmd = new MySqlCommand(query))
                {
                    cmd.Connection = sqlcon;
                    sqlcon.Open();
                    cmd.ExecuteScalar();
                }
                query = "UPDATE dns_module SET nt_zone_id = (SELECT nt_zone_id from domain_names where zone = '" + dname + "'), state_id = 'Subscription' WHERE domain_name = '" + dname + "' AND country_code = '" + countryCode + "'";
                using (MySqlCommand cmd = new MySqlCommand(query))
                {
                    cmd.Connection = sqlcon;
                    cmd.ExecuteScalar();
                }
            }

            MailMessage msg = new MailMessage();

            msg.To.Add(new MailAddress("chinwk_95@hotmail.com"));
            msg.From = new MailAddress("mindserver15@gmail.com");
            msg.Subject = "Company Validation";
            msg.Body = "Congratulation, Your Company Validation is Successful";
            msg.IsBodyHtml = true;

            using (SmtpClient smtp = new SmtpClient())
            {
                NetworkCredential credential = new NetworkCredential("mindserver15@gmail.com", "proxyguy123");
                smtp.Host = "smtp.gmail.com";
                smtp.Port = 587;
                smtp.EnableSsl = true;
                smtp.Credentials = credential;
                smtp.Send(msg);
            }
            return RedirectToAction("Validate", "Admin", FormMethod.Post);
        }

        public ActionResult Reject(string dname, string countryCode)
        {
            using (MySqlConnection sqlcon = new MySqlConnection(strcon))
            {
                string dbTable, dnsID, genericBusiness_id;
                string query = "SELECT y.business_type, y.dns_id FROM customer x JOIN dns_module y ON x.customer_id = y.customer_id WHERE y.domain_name = '" + dname + "' AND y.country_code = '" + countryCode + "'";
                using (MySqlCommand cmd = new MySqlCommand(query))
                {
                    cmd.Connection = sqlcon;
                    sqlcon.Open();
                    using (MySqlDataReader reader = cmd.ExecuteReader())
                    {
                        reader.Read();
                        dbTable = reader[0].ToString();
                        dnsID = reader[1].ToString();
                    }
                }
                query = "SELECT x." + dbTable.ToLower() + "_id FROM " + dbTable.ToLower() + " x WHERE dns_id = (SELECT y.dns_id FROM dns_module y WHERE y.domain_name = '" + dname + "' AND y.country_code = '" + countryCode + "')";
                using (MySqlCommand cmd = new MySqlCommand(query))
                {
                    cmd.Connection = sqlcon;
                    using (MySqlDataReader reader = cmd.ExecuteReader())
                    {
                        reader.Read();
                        genericBusiness_id = reader[0].ToString();
                    }
                }
                /*query = "DELETE FROM " + dbTable.ToLower() + " WHERE dns_id = (SELECT y.dns_id FROM dns_module y WHERE y.domain_name = '" + dname + "' AND y.country_code = '" + countryCode + "')";
                using (MySqlCommand cmd = new MySqlCommand(query))
                {
                    cmd.Connection = sqlcon;
                    cmd.ExecuteScalar();
                }
                query = "DELETE FROM dns_module WHERE domain_name = '" + dname + "' AND country_code = '" + countryCode + "'";
                using (MySqlCommand cmd = new MySqlCommand(query))
                {
                    cmd.Connection = sqlcon;
                    cmd.ExecuteScalar();
                }*/
                query = "CREATE EVENT IF NOT EXISTS Delete_" + genericBusiness_id + " ON SCHEDULE AT '" + DateTime.Now.AddDays(7).ToString("yyyy-MM-dd HH:mm:ss") + "' DO DELETE FROM " + dbTable.ToLower() + " WHERE dns_id = '" + dnsID + "'";
                using (MySqlCommand cmd = new MySqlCommand(query))
                {
                    cmd.Connection = sqlcon;
                    cmd.ExecuteScalar();
                }
                query = "CREATE EVENT IF NOT EXISTS Delete_" + dnsID + " ON SCHEDULE AT '" + DateTime.Now.AddDays(7).ToString("yyyy-MM-dd HH:mm:ss") + "' DO DELETE FROM dns_module WHERE dns_id = '" + dnsID + "'";
                using (MySqlCommand cmd = new MySqlCommand(query))
                {
                    cmd.Connection = sqlcon;
                    cmd.ExecuteScalar();
                }
            } 

            MailMessage msg = new MailMessage();

            msg.To.Add(new MailAddress("chinwk_95@hotmail.com"));
            msg.From = new MailAddress("mindserver15@gmail.com");
            msg.Subject = "Company Validation";
            msg.Body = "Sorry, Your Company Validation is Unsuccessful.  Please visit the following link: <a href=\"http://localhost:53429/Home/Contact \">Here</a>";
            msg.IsBodyHtml = true;

            using (SmtpClient smtp = new SmtpClient())
            {
                NetworkCredential credential = new NetworkCredential("mindserver15@gmail.com", "proxyguy123");
                smtp.Host = "smtp.gmail.com";
                smtp.Port = 587;
                smtp.EnableSsl = true;
                smtp.Credentials = credential;
                smtp.Send(msg);
            }
            return RedirectToAction("Validate", "Admin", FormMethod.Post);
        }
    }
}