namespace WebApplication2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateRegistration : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "CompanyName", c => c.String(unicode: false));
            AddColumn("dbo.AspNetUsers", "CompanyRegNum", c => c.String(unicode: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "CompanyRegNum");
            DropColumn("dbo.AspNetUsers", "CompanyName");
        }
    }
}
