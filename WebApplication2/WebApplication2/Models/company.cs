namespace WebApplication2.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("databasee.company")]
    public partial class company
    {
        [Key]
        [StringLength(20)]
        public string company_id { get; set; }

        [StringLength(20)]
        public string company_registration_number { get; set; }

        [StringLength(20)]
        public string company_name { get; set; }

        [StringLength(100)]
        public string company_address { get; set; }

        public int? company_phone_number { get; set; }

        [StringLength(20)]
        public string company_email { get; set; }

        [Column(TypeName = "char")]
        [StringLength(50)]
        public string owner_name { get; set; }

        public int? owner_phone_number { get; set; }

        [StringLength(20)]
        public string owner_email { get; set; }

        [StringLength(20)]
        public string owner_ic_number { get; set; }

        public int? customer_id { get; set; }

        [Column(TypeName = "enum")]
        [Required]
        [StringLength(65532)]
        public string business_type { get; set; }

        public virtual businesstype businesstype { get; set; }
    }
}
