namespace WebApplication2.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("databasee.na")]
    public partial class na
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int na_id { get; set; }

        [Required]
        [StringLength(20)]
        public string na_name { get; set; }

        [Required]
        [StringLength(20)]
        public string na_ic { get; set; }

        [Required]
        [StringLength(20)]
        public string na_email { get; set; }

        public int na_phone_number { get; set; }

        [Required]
        [StringLength(20)]
        public string customer_id { get; set; }

        [Column(TypeName = "enum")]
        [Required]
        [StringLength(65532)]
        public string business_type { get; set; }

        public virtual businesstype businesstype { get; set; }

        public virtual customer customer { get; set; }
    }
}
