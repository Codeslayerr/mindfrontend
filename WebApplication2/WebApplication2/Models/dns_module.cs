namespace WebApplication2.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("databasee.dns_module")]
    public partial class dns_module
    {
        [Key]
        [StringLength(20)]
        public string dns_id { get; set; }

        [Required]
        [StringLength(50)]
        public string domain_name { get; set; }

        [Required]
        [StringLength(3)]
        public string country_code { get; set; }

        [Column(TypeName = "timestamp")]
        public DateTime? purchase_date { get; set; }

        [Column(TypeName = "timestamp")]
        public DateTime? expiration_date { get; set; }

        [Required]
        [StringLength(20)]
        public string customer_id { get; set; }

        [Column(TypeName = "enum")]
        [Required]
        [StringLength(65532)]
        public string state_id { get; set; }

        public int? nt_zone_id { get; set; }

        public int? nt_zone_record_id { get; set; }

        public int? nt_zone_log_id { get; set; }

        public int? nt_zone_record_log_id { get; set; }

        public int? ttl { get; set; }

        [Column(TypeName = "enum")]
        [Required]
        [StringLength(65532)]
        public string business_type { get; set; }

        public virtual businesstype businesstype { get; set; }

        public virtual customer customer { get; set; }
    }
}
