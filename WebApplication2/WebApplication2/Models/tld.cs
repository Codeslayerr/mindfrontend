namespace WebApplication2.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("databasee.tld")]
    public partial class tld
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int tld_id { get; set; }

        [Required]
        [StringLength(10)]
        public string tld_name { get; set; }

        [Column(TypeName = "enum")]
        [Required]
        [StringLength(65532)]
        public string business_type { get; set; }

        public virtual businesstype businesstype { get; set; }
    }
}
