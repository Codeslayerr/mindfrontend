namespace WebApplication2.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("databasee.payment_transactions")]
    public partial class payment_transactions
    {
        [Key]
        [StringLength(20)]
        public string transaction_id { get; set; }

        public int? payment_method { get; set; }

        [Column(TypeName = "timestamp")]
        public DateTime? transaction_date { get; set; }

        public double? amount { get; set; }

        [Column(TypeName = "char")]
        [StringLength(5)]
        public string currency_code { get; set; }
    }
}
