namespace WebApplication2.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("databasee.partnership")]
    public partial class partnership
    {
        [Key]
        [StringLength(20)]
        public string partnership_id { get; set; }

        [Required]
        [StringLength(20)]
        public string partnership_name { get; set; }

        [Required]
        [StringLength(20)]
        public string partnership_registration_number { get; set; }

        [Required]
        [StringLength(20)]
        public string partnership_primary_name { get; set; }

        [Required]
        [StringLength(20)]
        public string partnership_email { get; set; }

        public int partnership_phone_number { get; set; }

        [Required]
        [StringLength(20)]
        public string customer_id { get; set; }

        [Column(TypeName = "char")]
        [Required]
        [StringLength(2)]
        public string cust_type { get; set; }

        [Column(TypeName = "enum")]
        [Required]
        [StringLength(65532)]
        public string business_type { get; set; }

        public virtual businesstype businesstype { get; set; }

        public virtual customer customer { get; set; }
    }
}
