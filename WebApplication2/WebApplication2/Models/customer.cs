namespace WebApplication2.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("databasee.customer")]
    public partial class customer
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public customer()
        {
            barcouncil = new HashSet<barcouncil>();
            cpa = new HashSet<cpa>();
            dns_module = new HashSet<dns_module>();
            na = new HashSet<na>();
            pam = new HashSet<pam>();
            partnership = new HashSet<partnership>();
            soleproprietor = new HashSet<soleproprietor>();
        }

        [Key]
        [StringLength(20)]
        public string customer_id { get; set; }

        [Column(TypeName = "char")]
        [Required]
        [StringLength(40)]
        public string cust_name { get; set; }

        [Required]
        [StringLength(20)]
        public string cust_username { get; set; }

        [Required]
        [StringLength(20)]
        public string cust_email { get; set; }

        public int cust_phone_number { get; set; }

        [Required]
        [StringLength(100)]
        public string cust_address { get; set; }

        [Required]
        [StringLength(15)]
        public string cust_password { get; set; }

        [StringLength(20)]
        public string transaction_id { get; set; }

        [Column(TypeName = "enum")]
        [Required]
        [StringLength(65532)]
        public string business_type { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<barcouncil> barcouncil { get; set; }

        public virtual businesstype businesstype { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cpa> cpa { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dns_module> dns_module { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<na> na { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pam> pam { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<partnership> partnership { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<soleproprietor> soleproprietor { get; set; }
    }
}
