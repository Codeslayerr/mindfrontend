namespace WebApplication2.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("databasee.subdomains_log")]
    public partial class subdomains_log
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int nt_zone_record_log_id { get; set; }

        public int nt_zone_id { get; set; }

        public int nt_user_id { get; set; }

        [Column(TypeName = "enum")]
        [Required]
        [StringLength(65532)]
        public string action { get; set; }

        public int timestamp { get; set; }

        public int nt_zone_record_id { get; set; }

        [StringLength(255)]
        public string name { get; set; }

        public int? ttl { get; set; }

        [StringLength(255)]
        public string description { get; set; }

        public short type_id { get; set; }

        [StringLength(512)]
        public string address { get; set; }

        public short? weight { get; set; }

        public short? priority { get; set; }

        [StringLength(255)]
        public string other { get; set; }

        [StringLength(20)]
        public string location { get; set; }
    }
}
