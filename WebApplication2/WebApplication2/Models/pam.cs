namespace WebApplication2.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("databasee.pam")]
    public partial class pam
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int pam_id { get; set; }

        [Required]
        [StringLength(20)]
        public string pam_name { get; set; }

        [Required]
        [StringLength(20)]
        public string pam_registration_number { get; set; }

        [Required]
        [StringLength(20)]
        public string pam_primary_name { get; set; }

        [Required]
        [StringLength(20)]
        public string pam_email { get; set; }

        public int pam_phone_number { get; set; }

        [Required]
        [StringLength(20)]
        public string customer_id { get; set; }

        [Column(TypeName = "enum")]
        [Required]
        [StringLength(65532)]
        public string business_type { get; set; }

        public virtual businesstype businesstype { get; set; }

        public virtual customer customer { get; set; }
    }
}
