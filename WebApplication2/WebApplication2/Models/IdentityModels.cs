﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace WebApplication2.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public string AuthyId { get; set; }
        public string AuthyStatus { get; set; }
        public string FullName { get; set; }
        public string IdentityCard { get; set; }
        public string CompanyName { get; set; }
        public string CompanyRegNum { get; set; }
        public string Country { get; set; }

        // Concatenate the company info for display in tables and such:
        public string DisplayAddress
        {
            get
            {
                string dspCompanyName = string.IsNullOrWhiteSpace(this.CompanyName) ? "" : this.CompanyName;
                string dspCompanyRegNum = string.IsNullOrWhiteSpace(this.CompanyRegNum) ? "" : this.CompanyRegNum;
                string dspCountry = string.IsNullOrWhiteSpace(this.Country) ? "" : this.Country;
                return string.Format("{0} {1} {2}", dspCompanyName, dspCompanyRegNum, dspCountry);
            }
        }

        public async Task<ClaimsIdentity>
            GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            var userIdentity = await manager
                .CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            return userIdentity;
        }
    }

    public class ApplicationRole : IdentityRole
    {
        public ApplicationRole() : base() { }
        public ApplicationRole(string name) : base(name) { }
        public string Description { get; set; }

    }
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }
}