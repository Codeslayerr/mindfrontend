namespace WebApplication2.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("databasee.domain_names")]
    public partial class domain_names
    {
        [Key]
        public int nt_zone_id { get; set; }

        public int? nt_group_id { get; set; }

        [StringLength(255)]
        public string zone { get; set; }

        [StringLength(127)]
        public string mailaddr { get; set; }

        [StringLength(255)]
        public string description { get; set; }

        public int? serial { get; set; }

        public int? refresh { get; set; }

        public int? retry { get; set; }

        public int? expire { get; set; }

        public int? minimum { get; set; }

        public int? ttl { get; set; }

        [StringLength(20)]
        public string location { get; set; }

        [Column(TypeName = "timestamp")]
        public DateTime? last_modified { get; set; }

        [Column(TypeName = "timestamp")]
        public DateTime? last_publish { get; set; }

        public bool? deleted { get; set; }
    }
}
