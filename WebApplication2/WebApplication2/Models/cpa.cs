namespace WebApplication2.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("databasee.cpa")]
    public partial class cpa
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int cpa_id { get; set; }

        [Required]
        [StringLength(20)]
        public string cpa_name { get; set; }

        [Required]
        [StringLength(20)]
        public string cpa_registration_number { get; set; }

        [Required]
        [StringLength(20)]
        public string cpa_primary_name { get; set; }

        [Required]
        [StringLength(20)]
        public string cpa_email { get; set; }

        public int cpa_phone_number { get; set; }

        [Required]
        [StringLength(20)]
        public string customer_id { get; set; }

        [Column(TypeName = "enum")]
        [Required]
        [StringLength(65532)]
        public string business_type { get; set; }

        public virtual businesstype businesstype { get; set; }

        public virtual customer customer { get; set; }
    }
}
