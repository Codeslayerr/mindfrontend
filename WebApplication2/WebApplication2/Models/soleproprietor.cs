namespace WebApplication2.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("databasee.soleproprietor")]
    public partial class soleproprietor
    {
        [Key]
        [StringLength(20)]
        public string soleproprietor_id { get; set; }

        [Required]
        [StringLength(20)]
        public string soleproprietor_name { get; set; }

        [Required]
        [StringLength(20)]
        public string soleproprietor_registration_number { get; set; }

        [Required]
        [StringLength(20)]
        public string soleproprietor_primary_name { get; set; }

        [Required]
        [StringLength(20)]
        public string soleproprietor_email { get; set; }

        public int soleproprietor_phone_number { get; set; }

        [Required]
        [StringLength(20)]
        public string customer_id { get; set; }

        [Column(TypeName = "enum")]
        [Required]
        [StringLength(65532)]
        public string business_type { get; set; }

        public virtual businesstype businesstype { get; set; }

        public virtual customer customer { get; set; }
    }
}
