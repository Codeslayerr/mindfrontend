﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication2.Models
{
    public class ItemDetails
    {
        public string Domain { get; set; }
        public string Name { get; set; }
        public string RegNum {get;set;}
        public string OwnerName { get; set; }
        public string Address { get; set; }
        public int PhoneNum { get; set; }
        public string Email { get; set; }
        public int OwnerPhoneNum { get; set; }
        public string OwnerEmail { get; set; }
        public string ICNum { get; set; }
        public bool Edit { get; set; }
        public List<ItemDetails> detailList { get; set; }

        public ItemDetails()
        {
            this.Edit = true;
        }

        public ItemDetails(string cartitem)
        {
            this.Domain = cartitem;
            this.Edit = true;
        }
    }
}