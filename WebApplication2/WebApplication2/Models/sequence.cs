namespace WebApplication2.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("databasee.sequence")]
    public partial class sequence
    {
        [Key]
        [StringLength(50)]
        public string seq_name { get; set; }

        [Required]
        [StringLength(50)]
        public string seq_group { get; set; }

        [Column(TypeName = "uint")]
        public long seq_val { get; set; }

        [Column(TypeName = "char")]
        [Required]
        [StringLength(5)]
        public string seq_alpha { get; set; }

        public int seq_ascii { get; set; }
    }
}
