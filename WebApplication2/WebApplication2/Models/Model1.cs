namespace WebApplication2.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class Model1 : DbContext
    {
        public Model1()
            : base("name=Model1")
        {
        }

        public virtual DbSet<barcouncil> barcouncil { get; set; }
        public virtual DbSet<businesstype> businesstype { get; set; }
        public virtual DbSet<company> company { get; set; }
        public virtual DbSet<cpa> cpa { get; set; }
        public virtual DbSet<customer> customer { get; set; }
        public virtual DbSet<dns_module> dns_module { get; set; }
        public virtual DbSet<domain_name_log> domain_name_log { get; set; }
        public virtual DbSet<domain_names> domain_names { get; set; }
        public virtual DbSet<na> na { get; set; }
        public virtual DbSet<pam> pam { get; set; }
        public virtual DbSet<partnership> partnership { get; set; }
        public virtual DbSet<payment_transactions> payment_transactions { get; set; }
        public virtual DbSet<sequence> sequence { get; set; }
        public virtual DbSet<soleproprietor> soleproprietor { get; set; }
        public virtual DbSet<subdomains> subdomains { get; set; }
        public virtual DbSet<subdomains_log> subdomains_log { get; set; }
        public virtual DbSet<tld> tld { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<barcouncil>()
                .Property(e => e.barcouncil_id)
                .IsUnicode(false);

            modelBuilder.Entity<barcouncil>()
                .Property(e => e.barcouncil_name)
                .IsUnicode(false);

            modelBuilder.Entity<barcouncil>()
                .Property(e => e.barcouncil_registration_name)
                .IsUnicode(false);

            modelBuilder.Entity<barcouncil>()
                .Property(e => e.barcouncil_primary_name)
                .IsUnicode(false);

            modelBuilder.Entity<barcouncil>()
                .Property(e => e.bacouncil_email)
                .IsUnicode(false);

            modelBuilder.Entity<barcouncil>()
                .Property(e => e.customer_id)
                .IsUnicode(false);

            modelBuilder.Entity<barcouncil>()
                .Property(e => e.business_type)
                .IsUnicode(false);

            modelBuilder.Entity<businesstype>()
                .Property(e => e.business_type)
                .IsUnicode(false);

            modelBuilder.Entity<businesstype>()
                .Property(e => e.business_group)
                .IsUnicode(false);

            modelBuilder.Entity<businesstype>()
                .HasMany(e => e.barcouncil)
                .WithRequired(e => e.businesstype)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<businesstype>()
                .HasMany(e => e.company)
                .WithRequired(e => e.businesstype)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<businesstype>()
                .HasMany(e => e.cpa)
                .WithRequired(e => e.businesstype)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<businesstype>()
                .HasMany(e => e.customer)
                .WithRequired(e => e.businesstype)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<businesstype>()
                .HasMany(e => e.dns_module)
                .WithRequired(e => e.businesstype)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<businesstype>()
                .HasMany(e => e.na)
                .WithRequired(e => e.businesstype)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<businesstype>()
                .HasMany(e => e.pam)
                .WithRequired(e => e.businesstype)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<businesstype>()
                .HasMany(e => e.partnership)
                .WithRequired(e => e.businesstype)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<businesstype>()
                .HasMany(e => e.soleproprietor)
                .WithRequired(e => e.businesstype)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<businesstype>()
                .HasMany(e => e.tld)
                .WithRequired(e => e.businesstype)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<company>()
                .Property(e => e.company_id)
                .IsUnicode(false);

            modelBuilder.Entity<company>()
                .Property(e => e.company_registration_number)
                .IsUnicode(false);

            modelBuilder.Entity<company>()
                .Property(e => e.company_name)
                .IsUnicode(false);

            modelBuilder.Entity<company>()
                .Property(e => e.company_address)
                .IsUnicode(false);

            modelBuilder.Entity<company>()
                .Property(e => e.company_email)
                .IsUnicode(false);

            modelBuilder.Entity<company>()
                .Property(e => e.owner_name)
                .IsUnicode(false);

            modelBuilder.Entity<company>()
                .Property(e => e.owner_email)
                .IsUnicode(false);

            modelBuilder.Entity<company>()
                .Property(e => e.owner_ic_number)
                .IsUnicode(false);

            modelBuilder.Entity<company>()
                .Property(e => e.business_type)
                .IsUnicode(false);

            modelBuilder.Entity<cpa>()
                .Property(e => e.cpa_name)
                .IsUnicode(false);

            modelBuilder.Entity<cpa>()
                .Property(e => e.cpa_registration_number)
                .IsUnicode(false);

            modelBuilder.Entity<cpa>()
                .Property(e => e.cpa_primary_name)
                .IsUnicode(false);

            modelBuilder.Entity<cpa>()
                .Property(e => e.cpa_email)
                .IsUnicode(false);

            modelBuilder.Entity<cpa>()
                .Property(e => e.customer_id)
                .IsUnicode(false);

            modelBuilder.Entity<cpa>()
                .Property(e => e.business_type)
                .IsUnicode(false);

            modelBuilder.Entity<customer>()
                .Property(e => e.customer_id)
                .IsUnicode(false);

            modelBuilder.Entity<customer>()
                .Property(e => e.cust_name)
                .IsUnicode(false);

            modelBuilder.Entity<customer>()
                .Property(e => e.cust_username)
                .IsUnicode(false);

            modelBuilder.Entity<customer>()
                .Property(e => e.cust_email)
                .IsUnicode(false);

            modelBuilder.Entity<customer>()
                .Property(e => e.cust_address)
                .IsUnicode(false);

            modelBuilder.Entity<customer>()
                .Property(e => e.cust_password)
                .IsUnicode(false);

            modelBuilder.Entity<customer>()
                .Property(e => e.transaction_id)
                .IsUnicode(false);

            modelBuilder.Entity<customer>()
                .Property(e => e.business_type)
                .IsUnicode(false);

            modelBuilder.Entity<customer>()
                .HasMany(e => e.barcouncil)
                .WithRequired(e => e.customer)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<customer>()
                .HasMany(e => e.cpa)
                .WithRequired(e => e.customer)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<customer>()
                .HasMany(e => e.dns_module)
                .WithRequired(e => e.customer)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<customer>()
                .HasMany(e => e.na)
                .WithRequired(e => e.customer)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<customer>()
                .HasMany(e => e.pam)
                .WithRequired(e => e.customer)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<customer>()
                .HasMany(e => e.partnership)
                .WithRequired(e => e.customer)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<customer>()
                .HasMany(e => e.soleproprietor)
                .WithRequired(e => e.customer)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<dns_module>()
                .Property(e => e.dns_id)
                .IsUnicode(false);

            modelBuilder.Entity<dns_module>()
                .Property(e => e.domain_name)
                .IsUnicode(false);

            modelBuilder.Entity<dns_module>()
                .Property(e => e.country_code)
                .IsUnicode(false);

            modelBuilder.Entity<dns_module>()
                .Property(e => e.customer_id)
                .IsUnicode(false);

            modelBuilder.Entity<dns_module>()
                .Property(e => e.state_id)
                .IsUnicode(false);

            modelBuilder.Entity<dns_module>()
                .Property(e => e.business_type)
                .IsUnicode(false);

            modelBuilder.Entity<domain_name_log>()
                .Property(e => e.action)
                .IsUnicode(false);

            modelBuilder.Entity<domain_name_log>()
                .Property(e => e.zone)
                .IsUnicode(false);

            modelBuilder.Entity<domain_name_log>()
                .Property(e => e.mailaddr)
                .IsUnicode(false);

            modelBuilder.Entity<domain_name_log>()
                .Property(e => e.description)
                .IsUnicode(false);

            modelBuilder.Entity<domain_names>()
                .Property(e => e.zone)
                .IsUnicode(false);

            modelBuilder.Entity<domain_names>()
                .Property(e => e.mailaddr)
                .IsUnicode(false);

            modelBuilder.Entity<domain_names>()
                .Property(e => e.description)
                .IsUnicode(false);

            modelBuilder.Entity<domain_names>()
                .Property(e => e.location)
                .IsUnicode(false);

            modelBuilder.Entity<na>()
                .Property(e => e.na_name)
                .IsUnicode(false);

            modelBuilder.Entity<na>()
                .Property(e => e.na_ic)
                .IsUnicode(false);

            modelBuilder.Entity<na>()
                .Property(e => e.na_email)
                .IsUnicode(false);

            modelBuilder.Entity<na>()
                .Property(e => e.customer_id)
                .IsUnicode(false);

            modelBuilder.Entity<na>()
                .Property(e => e.business_type)
                .IsUnicode(false);

            modelBuilder.Entity<pam>()
                .Property(e => e.pam_name)
                .IsUnicode(false);

            modelBuilder.Entity<pam>()
                .Property(e => e.pam_registration_number)
                .IsUnicode(false);

            modelBuilder.Entity<pam>()
                .Property(e => e.pam_primary_name)
                .IsUnicode(false);

            modelBuilder.Entity<pam>()
                .Property(e => e.pam_email)
                .IsUnicode(false);

            modelBuilder.Entity<pam>()
                .Property(e => e.customer_id)
                .IsUnicode(false);

            modelBuilder.Entity<pam>()
                .Property(e => e.business_type)
                .IsUnicode(false);

            modelBuilder.Entity<partnership>()
                .Property(e => e.partnership_id)
                .IsUnicode(false);

            modelBuilder.Entity<partnership>()
                .Property(e => e.partnership_name)
                .IsUnicode(false);

            modelBuilder.Entity<partnership>()
                .Property(e => e.partnership_registration_number)
                .IsUnicode(false);

            modelBuilder.Entity<partnership>()
                .Property(e => e.partnership_primary_name)
                .IsUnicode(false);

            modelBuilder.Entity<partnership>()
                .Property(e => e.partnership_email)
                .IsUnicode(false);

            modelBuilder.Entity<partnership>()
                .Property(e => e.customer_id)
                .IsUnicode(false);

            modelBuilder.Entity<partnership>()
                .Property(e => e.cust_type)
                .IsUnicode(false);

            modelBuilder.Entity<partnership>()
                .Property(e => e.business_type)
                .IsUnicode(false);

            modelBuilder.Entity<payment_transactions>()
                .Property(e => e.transaction_id)
                .IsUnicode(false);

            modelBuilder.Entity<payment_transactions>()
                .Property(e => e.currency_code)
                .IsUnicode(false);

            modelBuilder.Entity<sequence>()
                .Property(e => e.seq_name)
                .IsUnicode(false);

            modelBuilder.Entity<sequence>()
                .Property(e => e.seq_group)
                .IsUnicode(false);

            modelBuilder.Entity<sequence>()
                .Property(e => e.seq_alpha)
                .IsUnicode(false);

            modelBuilder.Entity<soleproprietor>()
                .Property(e => e.soleproprietor_id)
                .IsUnicode(false);

            modelBuilder.Entity<soleproprietor>()
                .Property(e => e.soleproprietor_name)
                .IsUnicode(false);

            modelBuilder.Entity<soleproprietor>()
                .Property(e => e.soleproprietor_registration_number)
                .IsUnicode(false);

            modelBuilder.Entity<soleproprietor>()
                .Property(e => e.soleproprietor_primary_name)
                .IsUnicode(false);

            modelBuilder.Entity<soleproprietor>()
                .Property(e => e.soleproprietor_email)
                .IsUnicode(false);

            modelBuilder.Entity<soleproprietor>()
                .Property(e => e.customer_id)
                .IsUnicode(false);

            modelBuilder.Entity<soleproprietor>()
                .Property(e => e.business_type)
                .IsUnicode(false);

            modelBuilder.Entity<subdomains>()
                .Property(e => e.name)
                .IsUnicode(false);

            modelBuilder.Entity<subdomains>()
                .Property(e => e.description)
                .IsUnicode(false);

            modelBuilder.Entity<subdomains>()
                .Property(e => e.address)
                .IsUnicode(false);

            modelBuilder.Entity<subdomains>()
                .Property(e => e.other)
                .IsUnicode(false);

            modelBuilder.Entity<subdomains>()
                .Property(e => e.location)
                .IsUnicode(false);

            modelBuilder.Entity<subdomains_log>()
                .Property(e => e.action)
                .IsUnicode(false);

            modelBuilder.Entity<subdomains_log>()
                .Property(e => e.name)
                .IsUnicode(false);

            modelBuilder.Entity<subdomains_log>()
                .Property(e => e.description)
                .IsUnicode(false);

            modelBuilder.Entity<subdomains_log>()
                .Property(e => e.address)
                .IsUnicode(false);

            modelBuilder.Entity<subdomains_log>()
                .Property(e => e.other)
                .IsUnicode(false);

            modelBuilder.Entity<subdomains_log>()
                .Property(e => e.location)
                .IsUnicode(false);

            modelBuilder.Entity<tld>()
                .Property(e => e.tld_name)
                .IsUnicode(false);

            modelBuilder.Entity<tld>()
                .Property(e => e.business_type)
                .IsUnicode(false);
        }
    }
}
